# frozen_string_literal: true

class AddressesController < ApplicationController
  def index
    @addresses = Address.all
    @search_addresses = []
    @search_addresses = AddressSearchService.find(params[:address].as_json) if params[:address]
  end

  def show
    @address = Address.find(params[:id])
    @search_address = AddressSearchService.get(@address.address_id) if @address.address_id.present?
  end

  # POST /invoice_configurations or /invoice_configurations.json
  def create
    @address = Address.new(address_params)
    respond_to do |format|
      if @address.save
        format.html { redirect_to [:addresses] }
      else
        format.html { render 'addresses/index', status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /invoice_configurations/1 or /invoice_configurations/1.json
  def update
    respond_to do |format|
      if @address.update(address_params)
        format.html { redirect_to [:addresses] }
      else
        format.html { render :index, status: :unprocessable_entity }
      end
    end
  end

  private

  def address_params
    params.require(:address).permit(:address_id, :lot_or_unit, :street_name, :house_name_or_number, :zipcode, :summary)
  end
end
