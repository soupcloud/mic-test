class AddressFormater
  attr_accessor :address
  def initialize(address)
    @address = address
  end

  def lot_or_unit
    "#{address["lot"]} #{address["unit"]} #{address["floor"]} #{address["floor_type"]}"
  end

  def house_name_or_number
    "#{address["house_number"]} #{address["property_name"]} #{address["street_number"]}"
  end

  def street_name
    "#{address["street_name"]} #{address["street_type"]} #{address["street_suffix"]}"
  end

  def zipcode
    address["postcode"]
  end

  def summary
    "#{lot_or_unit} #{house_name_or_number} #{street_name} #{zipcode}"
  end
end
