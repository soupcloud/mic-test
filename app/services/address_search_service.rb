class AddressSearchService
  require 'uri'
  require 'net/http'

  def self.get(id)
    result = []
    uri = URI("https://msats.micapp.io/mirns/#{id}")
    uri.query = URI.encode_www_form({ 'key' => Rails.configuration.api_key })
    res = Net::HTTP.get_response(uri)
    result = JSON.parse res.body if res.is_a?(Net::HTTPSuccess)
    result
  end

  def self.find(search)
    result = []
    search.merge!({ 'key' => Rails.configuration.api_key })
    uri = URI("https://msats.micapp.io/mirn_address")
    uri.query = URI.encode_www_form(search)
    res = Net::HTTP.get_response(uri)
    result = JSON.parse res.body if res.is_a?(Net::HTTPSuccess)
    result
  end
end
